#include "SimplePrint.h"
#include <stdio.h>


void red () {
    printf("%s", "\033[1;31m");
}

void yellow() {
    printf("%s", "\033[1;33m");
}

void green(){
    printf("%s", "\033[0;32m");
}

void reset () {
    printf("%s", "\033[0m");
}

void simplePrintWithColour(const char* message, int colour){
    switch (colour){
        case SP_C_RED:
            red();
            break;
        case SP_C_YELLOW:
            yellow();
            break;
        case SP_C_GREEN:
            green();
            break;
        default:
            reset();
            break;
    }
    printf("%s",message);
    reset();
}

void simplePrintError(const char* message){
    simplePrintWithColour("ERROR: ", SP_C_RED);
    simplePrintWithColour(message, SP_C_DEFAULT);
    printf("%s", "\n");
}

void simplePrintWarning(const char* message){
    simplePrintWithColour("WARNING: ", SP_C_YELLOW);
    simplePrintWithColour(message, SP_C_DEFAULT);
    printf("%s", "\n");
}

void simplePrintSuccess(const char* message){
    simplePrintWithColour(message, SP_C_GREEN);
    printf("%s", "\n");
}

void simplePrint(const char* message){
    reset();
    printf("%s", message);
    printf("%s", "\n");
}

/**
 CODE	COLOR
[0;31m	Red
[1;31m	Bold Red
[0;32m	Green
[1;32m	Bold Green
[0;33m	Yellow
[01;33m	Bold Yellow
[0;34m	Blue
[1;34m	Bold Blue
[0;35m	Magenta
[1;35m	Bold Magenta
[0;36m	Cyan
[1;36m	Bold Cyan
[0m	Reset
*/