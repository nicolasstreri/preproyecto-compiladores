#include "types.h"
#include <string.h>
#include <stddef.h>
#include <stdlib.h>

/**
 * List of symbol tables
*/
typedef struct DataLinkedList {} DataLinkedList; // TODO it will look what its shape

/*
	Function pointer that compares two nodes' data of the a linked list
	@return 1 is equal, 0 otherwise

	NO IMPLEMENTAR ACA. SI NO ENTENDER PEDIR EXPLICACION A NICO
*/
typedef int (*EqualDataLinkedListFunction)(DataLinkedList, DataLinkedList); 

typedef struct NodeLinkedList {
	struct DataLinkedList* data;
	struct NodeLinkedList* next;
} NodeLinkedList;


typedef struct LinkedList {
	int size;
	struct NodeLinkedList* head;
	struct NodeLinkedList* end;
} LinkedList;

/**
 * Create a new LinkedList
 * @return Pointer to LinkedList
*/
LinkedList* newLinkedList(){
	LinkedList* newList = malloc(sizeof(LinkedList));
	newList->size = 0;
	newList->head = NULL;
	newList->end = NULL;
	return newList;
}

/**
 * Verifies if the given position is valid
 * @pre list != NULL
 * @param list : The list to check the position
 * @param pos : The position of the list
 * @return int : 1 if the position is valid, 0 otherwise
 */
int isValidPosition (LinkedList* list, int pos) {
	if (pos < 0 || pos > list->size) {
		printf("The position is over range\n");
		return 0;
	}
	return 1;
}

/**
 * Insert data in a specific given position
 * @pre list != NULL
 * @param list : The list where the data is inserted
 * @param pos : The position to insert the data
 * @param data : The data  to be inserted
*/
void insert_LL(LinkedList* list, int pos, DataLinkedList* data){
	if (!isValidPosition(list, pos)) return;

	// Creates a new node
	NodeLinkedList* newNode = malloc(sizeof(NodeLinkedList));
	newNode->data = data;

	list->size ++;
	// Searchs position
	if (pos == 0) {  //Begin and insertion in Empty list
		newNode->next = list->head;
		list->head = newNode;

		if(list->end == NULL) list->end = newNode;
		return;
	} 
	if (pos == list->size){ //End
		newNode->next = NULL;
		list->end->next = newNode;
		list->end = newNode;
		return;
	}

	//Middle
	// @inv 0 < pos < list->size
	int i = 0;
	NodeLinkedList* auxHead = list->head;
	while (i < pos-1) {
		auxHead = auxHead->next;
		i++;
	}
	newNode->next = auxHead->next;
	auxHead->next = newNode;
}

/**
 * Insert data at the beginning of a list
 * @see insert_LL function
*/
void insertFirst_LL(LinkedList* list, DataLinkedList* data){
	insert_LL(list, 0, data);
}

/**
 * Insert data at the end of a list
 * @see insert_LL function
*/
void insertEnd_LL(LinkedList* list, DataLinkedList* data){
	insert_LL(list, list->size, data);
}

/**
 * Search the pos of data in a Linked List
 * @param list : the list where the search will be performed
 * @param data : The pointer to the data to search
 * @param isEqual : A function that it will use to compare the 'data' parameter with data in the list
 * @return position of the first occurrence of the data, or -1 if it does not exist.
*/
int search_LL(LinkedList* list, DataLinkedList* data, EqualDataLinkedListFunction isEqual){
	// TODO
	// Use for compare two data of node the invocation: isEqual(data1, data2);
}

/**
 * Gets data of the List
 * @param list
 * @param pos : position of the data to obtain
 * @return Pointer to the data, or NULL if not exists the position in List
*/
DataLinkedList* getData_LL(LinkedList* list, int pos){
	if(!isValidPosition(list, pos)) return NULL;

	int i = 0;
	NodeLinkedList* auxHead = list->head;
	while (i < pos) {
		auxHead = auxHead->next;
		i++;
	}
	return auxHead;
}

/**
 * Delete the data in one position.
 * @param list : the list where the deletion will be performed
 * @param pos : position to eliminate
*/
void remove_LL(LinkedList* list, int pos){
	if (!isValidPosition(list, pos)) return;
	if (pos == list->size) {
		printf("The position is over range");
	};

	list->size --;

	NodeLinkedList* auxHead = list->head;
	if (pos == 0) { //Begin of the list
		list->head = auxHead->next;

		//The list only contains one element
		if (pos == list->size) list->end = NULL; 

		free(auxHead);
		return;
	}

	int i = 0;
	while (i < pos-1) {
		auxHead = auxHead->next;
		i++;
	}
	auxHead->next = auxHead->next->next;

	//Update End of the list
	if (pos == list->size) list->end = auxHead;
	
	free(auxHead->next);
	//@see Destroy the node using the 'free' function
}




LinkedList* headList = NULL;

nodeSymbolTable* createNodeTable(infoVar *var) {
	nodeSymbolTable* newNode;
    newNode = malloc(sizeof(nodeSymbolTable));
    newNode->info = var;
    newNode->next = NULL;
    return newNode;
}

infoVar* getVarTableList(list *tableList, char *nameVar) {
	if (tableList->headTableList == NULL) return NULL;
	nodeSymbolTable *auxHead = tableList->headTableList;
	while(auxHead != NULL) {
		if(strcmp(auxHead->info->name, nameVar) == 0) {
            return auxHead->info;
        }
		auxHead = auxHead->next;
	}
	return NULL;
}

int existVarInTable(list *tableList, char* nameVar){
   return getVarTableList(tableList, nameVar) != NULL;
}

void insertInTableList(list *tableList, infoVar *var) {
	nodeSymbolTable *newNode = createNodeTable(var);
	newNode->next = tableList->headTableList;
	tableList->headTableList = newNode;
}

int insertIfNotExist(list *list,infoVar* var){
    if(existVarInTable(list, var->name)) return 0;
    
    insertInTableList(list, var);
    return 1;
}

/**
 * @param nameTableList : Specify the type-name of the list
*/
list* newList(char *nameTableList) {
	list* newList;
	newList = malloc(sizeof(list));
	newList->nameTableList = nameTableList;
	newList->headTableList = NULL;
	newList->next = headList;
	headList = newList;
	return newList;
}

list* getList(char *nameTable){
	if (headList == NULL) return NULL;
	list *auxHeadList = headList;
	while(auxHeadList != NULL) {
		if(strcmp(auxHeadList->nameTableList, nameTable) == 0) {
            return auxHeadList;
        }
		auxHeadList = auxHeadList->next;
	}
	return NULL;
}

int insertInList(char *nameTableList, infoVar* var) {
	list *auxList = getList(nameTableList);
	if (auxList == NULL) auxList = newList(nameTableList);
	
	return insertIfNotExist(auxList, var);
}


