#ifndef __SIMPLE_PRINT__
#define __SIMPLE_PRINT__
/**
 * SimplePrint is a simple library useful for prints messages in the console.
 * Allows prints messages of type success, warning, error and default.
*/

//Colours supported
#define SP_C_DEFAULT 0
#define SP_C_RED 1
#define SP_C_YELLOW 2
#define SP_C_GREEN 3

/**
 * Prints an error message
 * @format [RED]Error: [/RED] message \n    // Note that it is added at the end \n
 * @param message : message to print
*/
void simplePrintError(const char* message);

/**
 * Prints an warning message
 * @format [YELLOW]Warning: [/YELLOW] message \n    // Note that it is added at the end \n
 * @param message : message to print
*/
void simplePrintWarning(const char* message);

/**
 * Prints an success message
 * @format [GREEN] message [/GREEN] \n    // Note that it is added at the end \n
 * @param message : message to print
*/
void simplePrintSuccess(const char* message);

/**
 * Prints an default message
 * @format message \n    // Note that it is added at the end \n
 * @param message : message to print
*/
void simplePrint(const char* message);

/**
 * Prints an message with colour
 * @format [COLOUR] message [/COLOUR]   // Note that it is not added at the end \n
 * @param message : message to print
 * @param colour : colour of the message
*/
void simplePrintWithColour(const char* message, int colour);

#endif // __SIMPLE_PRINT__
