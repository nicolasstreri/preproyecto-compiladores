%{

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "types.h"
#include "tree.h"
#include "symbolTable.h"
#include "messages.h"

// External
extern int yylineno;
extern nodeTree* astTree;

//Functions
void newVarDeclaration(char* varName, int varValue);
nodeTree* id(char* nameVar);
nodeTree* con(int value); // Constant: INT
nodeTree* opr(char operator, nodeTree* operandOne, nodeTree* operandTwo);

%}
 
%union { int i; char *s; struct nodeTree * t; }
 
%token<i> INT
%token<s> ID
%token VAR

%type<t> expr
 
%left '+' 
%left '*'
 
%%
 
prog: vars expr ';'     { astTree = $2; } 
    ;

vars: | varDeclaration vars

varDeclaration: 
    VAR ID '=' INT ';'  { newVarDeclaration($2, $4); }
    ;
  
expr:
    ID                  { $$ = id($1); markAsUsed($1); }
    | INT               { $$ = con($1); }
    | expr '+' expr     { $$ = opr('+', $1, $3); }
    | expr '*' expr     { $$ = opr('*', $1, $3);}
    | '(' expr ')'      { $$ =  $2; }
    ;
 
%%

/**
 * Process a new declaration of variable.
 * Adds in the Symbol table the new variable.
 *
 * @param varName : Name of the new Variable
 * @param varValue :  integer value that represents the variable
 * @note This function cancels the program if the variable is already defined
*/
void newVarDeclaration(char* varName, int varValue){
    infoVar *newVar;
    newVar = malloc(sizeof(infoVar));
    newVar->name = varName;
    newVar->value = varValue;
    newVar->declarationLine = yylineno;
                            
    if(!insertIfExistTable(newVar)){
        messageRedefinedVar(newVar);
    }
}

/**
 * Creates a new tree node of type Variable.
 * @param nameVar : name of variable that will be referenced by the node
 * @return a tree node
*/
nodeTree* id(char* nameVar){
    infoNodeTree* info = malloc(sizeof(infoNodeTree));
    info->type = NODE_VAR_TYPE;
    info->value.v = getVarTable(nameVar);
    if (info->value.v == NULL) {
        messageUndefinedVar(nameVar, yylineno);
    }
    return createLeafTree(info);
}

/**
 * Creates a new tree node of type Integer
 * @param value : integer that will be referenced by the node
 * @return a tree node
*/
nodeTree* con(int value){
    infoNodeTree* info = malloc(sizeof(infoNodeTree));
    info->type = NODE_INT_TYPE;
    info->value.i = value;
    return createLeafTree(info);
}

/**
 * Creates a new tree node of type Operator
 * @param operator : an operator identifier
 * @param operandOne : a tree that represents the value of the first operand
 * @param operandTwo : a tree that represents the value of the second operand
 * @return a tree node
 * @note: This function only works with binary operators.
*/
nodeTree* opr(char operator, nodeTree* operandOne, nodeTree* operandTwo){
    infoNodeTree* info = malloc(sizeof(infoNodeTree));
    info->type = NODE_OPERATOR_TYPE;
    info->value.o = operator;
    return createTree(info, operandOne, operandTwo);
}
