#ifndef _TREE_H_
#define _TREE_H_

#include "types.h"
#include <stdio.h>

/**
* Creates a new tree
* @param infoRoot : The information that the root node will contain
* @param leftTree : The left subtree of the new tree
* @param rightTree : The right subtree of the new tree
* @return nodeTree : A node representing the new tree created
*/
nodeTree* createTree(infoNodeTree* infoRoot, nodeTree* leftTree, nodeTree* rightTree);

/**
* Creates a leaf for a tree
* @param infoRoot : The information that the leaf node will contain
* @return nodeTree : A node represeting a leaf of the tree
*/
nodeTree* createLeafTree(infoNodeTree* infoRoot);

/**
* Performs a recursive postorder search of the tree to calculate its value
* @param tree : The tree to be search
* @return int : The value calculated of the tree
*/
int calculateResultTree(nodeTree* tree);

/**
 * Creates in a file the representation of a tree using dot language
 * @param f : The file where the representation is gonna be saved
 * @param tree : The tree to be represented
*/
void calculateDotTree(FILE *f, nodeTree* tree);

#endif
