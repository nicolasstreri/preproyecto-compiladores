#include "types.h"

/**
* Insert into the head of the table a variable
* @Param var : The variable to be inserted
*/
void insertTable(infoVar* var);

/**
* Check if exists a variable in the table
* @param nameVar : The name of the variable
* @return int : 1 if exists, 0 otherwise
*/
int existTable(char* nameVar);

/**
* Get a variable from the table
* @param nameVar : The name of the variable
* @return infoVar : The value of the variable
*/
infoVar* getVarTable(char* nameVar);

/**
* Insert a variable in the table only if the variable is not already contained
* @param var : The variable to be inserted
* @return int : 1 if the variable was inserted, 0 otherwise
*/
int insertIfExistTable(infoVar* var);

/**
 * Mark a variable in the symbol table as used
 * @param nameVar : The variable to be marked
*/
void markAsUsed(char* nameVar);

/**
 * Return the table that contains the variables
 * @return nodeSymbolTable : Head of the symbol table
*/
nodeSymbolTable* getSymbolTable();