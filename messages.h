#ifndef __MESSAGES__
#define __MESSAGES__

/**
 * Print a warning message when a variable was never used
 * @param var : The unused variable
*/
void messageUnusedVar(infoVar* var);

/**
 * Print an error message when a variable is undefined
 * @param var : The name of the undefined variable
 * @param line : The line where the variable was found
*/
void messageUndefinedVar(char* var, int line);

/**
 * Print an error message when finds a redefined variable
 * @param var : The redefined variable
*/
void messageRedefinedVar(infoVar* var);

#endif
