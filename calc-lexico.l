%{

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "calc-sintaxis.tab.h"

%}

%option noyywrap
%option yylineno

letra [a-zA-Z]
digito [0-9]

%%

{digito}+   { yylval.i = atoi(yytext); return INT; }

"var"   { return VAR; }

{letra}({letra}|{digito})*  { 
    yylval.s=(char *) malloc(sizeof(yytext)*yyleng);
    strcpy(yylval.s,yytext);
    return ID;
}

[+*;()=]    { return *yytext; }

.|\n    ; /* ignore all the rest */

%%


void yyerror(char const *s){
	printf("%s # In line: %d\n", s, yylineno);
}
