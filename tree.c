#include "tree.h"
#include <stdlib.h>
#include <stdio.h>
// External functions

nodeTree* createTree(infoNodeTree* infoRoot, nodeTree* leftTree, nodeTree* rightTree){
    nodeTree* newTree = malloc(sizeof(nodeTree));
    newTree->info = infoRoot;
    newTree->left = leftTree;
    newTree->right = rightTree;
    return newTree;
}

nodeTree* createLeafTree(infoNodeTree* infoRoot){
    return createTree(infoRoot, NULL, NULL);
}

int calculateResultTree(nodeTree* tree){
    switch(tree->info->type){
        case NODE_INT_TYPE: return tree->info->value.i;
        case NODE_VAR_TYPE: return tree->info->value.v->value;
        case NODE_OPERATOR_TYPE:
            switch(tree->info->value.o){
                case '+': return calculateResultTree(tree->left) + calculateResultTree(tree->right);
                case '*': return calculateResultTree(tree->left) * calculateResultTree(tree->right);
            }
    }
}

int getId(){
    static int id = 0;
    id++;
    return id;
}

int _calculateDotTree(FILE *f, nodeTree* tree){
    int id = getId();

     if(tree == NULL){
        fprintf(f, "%d [label = \"NULL\"] \n", id);
        return id;
     }

    switch(tree->info->type){
        case NODE_INT_TYPE: {
            fprintf(f, "%d [label = \"%d\"] \n", id, tree->info->value.i);
            return id;
        }
        case NODE_VAR_TYPE: {
            fprintf(f, "%d [label = \"%s : %d\"] \n", id, tree->info->value.v->name, tree->info->value.v->value);
            return id;
        }
        case NODE_OPERATOR_TYPE:{
            switch(tree->info->value.o){
                case '+': 
                        fprintf(f, "%d [label = \"+\"] \n", id);
                        break;
                case '*': 
                        fprintf(f, "%d [label = \"*\"] \n", id);
                        break;
            }
            int idLeft = _calculateDotTree(f, tree->left);
            int idRight = _calculateDotTree(f, tree->right); 
            fprintf(f, " %d -> { %d, %d } \n", id, idLeft, idRight);
            return id;
        }
    }
}

void calculateDotTree(FILE *f, nodeTree* tree) {
    fprintf(f, "digraph G {\n");
    fprintf(f, "inic[shape=point];\n");
    fprintf(f, "inic->%d;", _calculateDotTree(f, tree));
    fprintf(f, "}\n");
}
