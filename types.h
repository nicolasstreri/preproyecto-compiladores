#ifndef _TYPES_H_
#define _TYPES_H_

#include <stdbool.h>

#define NODE_INT_TYPE 1
#define NODE_OPERATOR_TYPE 2
#define NODE_VAR_TYPE 3

typedef struct infoVar {
    char* name;
    int value;
    int declarationLine;
    bool flagUsed;
} infoVar;

typedef struct nodeSymbolTable {
    infoVar *info;
    struct nodeSymbolTable *next;
} nodeSymbolTable;

typedef union nodeTypeUnion {
    int i;
    char o;
    infoVar* v;
} nodeTypeUnion;

typedef struct infoNodeTree {
    int type;
    int associatedLine;
    nodeTypeUnion value;
} infoNodeTree;

typedef struct nodeTree {
    infoNodeTree *info;
    struct nodeTree *left;
    struct nodeTree *right;    
} nodeTree;

#endif
