#include "symbolTable.h"
#include <string.h>
#include <stddef.h>
#include <stdlib.h>

nodeSymbolTable* head = NULL;

void insertTable(infoVar* var){
    //Creates a new node
    nodeSymbolTable* newNode;
    newNode = malloc(sizeof(nodeSymbolTable));
    newNode->info = var;
    newNode->next = head;
    
    //Updates head
    head = newNode;
}

nodeSymbolTable* getSymbolTable(){
    return head;
}

/**
 * Check if exists a nameVar in the table
 * Return: 1 if exist, 0 otherwise
*/
int existTable(char* nameVar){
   return getVarTable(nameVar) != NULL;
}

void markAsUsed(char* nameVar){
    infoVar* var = getVarTable(nameVar);
    if(var == NULL)return;

    var->flagUsed = true;
}


infoVar * getVarTable(char* nameVar){
    if(head == NULL) return NULL;
    
    nodeSymbolTable *cc = head;
    while(cc != NULL){
        if(strcmp(cc->info->name, nameVar) == 0) {
            return cc->info;
        }
        cc = cc->next;
    }
    return NULL;
}


int insertIfExistTable(infoVar* var){
    if(existTable(var->name)) return 0;
    
    insertTable(var);
    return 1;
}
