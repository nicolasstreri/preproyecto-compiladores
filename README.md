# Proyecto : Primera entrega

## Integrantes

* Elena, Pablo
* Streri, Nicolás

## Compilación
Para compilar el proyecto ejecutar el comando: 

>**./compile**

**Nota:** Requiere tener instalado: *flex, bison y gcc.*

## Uso
### Obtener resultado de una expresión
Para obtener el resultado de una expresión se tiene que ejecutar el comando:

>**./MTDSInterpreter miExpresion.txt**

Este comando muestra por pantalla el resultado de la expresión o los errores que surjan en el momento.

**Nota:** En la carpeta _**'test'**_ se encuentran expresiones de ejemplo para probar el programa.


### Obtener Árbol AST de una expresión
Para generar la representación de una expresión en forma de árbol sintáctico abstracto se debe ejecutar el comando:

>**./MTDSInterpreter miExpresion.txt -d outputAST.dot**

Este comando calcula el resultado de la expresión y, además, genera el archivo "outputAST.dot" con el cual se podra crear una imagen del árbol sintáctico abstracto.

## Visualizar AST
Utilizando el siguiente comando es posible obtener una imagen de la representación de la expresión:

>**dot outputAST.dot -Tpng -o arbol.png**

**Nota**: Es posible utilizar un visualizador de dot online [https://dreampuf.github.io/GraphvizOnline/]

## Ejemplo
Dada una expresión a ser evaluada, ésta es representada a través de un árbol. Para la siguiente expresion:

> var x = 5;
> var y = 3;
> x * y + 5;

se obtiene el Árbol: 
![Arbol](https://i.imgur.com/wxZ75ku.png)

## Consideraciones

La gramática que se utilizó es la siguiente:

>E' -> vars E ';'

>vars -> lambda 

>vars -> varDeclaration vars

>varDeclaration -> 'var' ID '=' INT ';'

>E -> E + E

>E -> E ∗ E

>E -> ( E )

>E -> INT

**NOTA:** 
- ID coincide con la ER **{letra}({letra}|{digito})**
- INT coincide con la ER: **[0-9]+**

donde: 
**letra** = [a-zA-Z]
**digito** = [0-9]