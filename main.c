/**
 * Main File : Mini-TDS Expression Interpreter
 * @author Nicolas Streri
 * @author Pablo Elena
 * @description An interpreter of expressions with variables. First part of the project of the subject 'Taller de diseno de software'
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stddef.h>
#include "types.h"
#include "tree.h"
#include "symbolTable.h"
#include "messages.h"
#include "SimplePrint.h"
#include "calc-sintaxis.tab.h"


nodeTree* astTree = NULL;
FILE* dotFile = NULL;
extern FILE *yyin;

/**
 * Prints the program usage options
*/
void usage(){
    simplePrint("Usage: <file> [-d FileOutput]");
    simplePrint("   -d : Generates a dot file with the representation of the AST tree");
    exit(0);
}

void checkUnusedVars(){
    nodeSymbolTable* table = getSymbolTable();
    while(table != NULL){
        if(!table->info->flagUsed){
            messageUnusedVar(table->info);
        }
        table = table->next;
    }
}

/**
 * Main function
*/
int main(int argc,char *argv[]){
	++argv,--argc;

    //Usage
    simplePrintSuccess("Mini-TDS Expression Interpreter\n");
    if(argc == 1){
        yyin = fopen(argv[0],"r");
    }else if(argc == 3 && strcmp(argv[1], "-d") == 0){
        yyin = fopen(argv[0],"r");
        dotFile = fopen(argv[2], "w");
    }else{
        usage(); return 0;
    }

    //Parse the input file
	yyparse();

    //Evaluate the AST Tree
    if(astTree == NULL) return 1;
    checkUnusedVars();
    int result = calculateResultTree(astTree);
    
    //Print result
    char* buf = malloc(sizeof(char) * 25);
    snprintf(buf, 25, "The result is: %d", result);
    simplePrint(buf);

    //Generate dot file
    if(dotFile != NULL){
        calculateDotTree(dotFile, astTree);
        fclose(dotFile);
    }
    return 0;
}
