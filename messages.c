#include "types.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "SimplePrint.h"

char* _createBuffer(int size) {
    char* buf = malloc(sizeof(char) * size);
}

void messageUnusedVar(infoVar* var){
    int size = 35 + strlen(var->name);
    char* buf = _createBuffer(size);

    snprintf(buf, size, "Variable: %s Unnused. # In line:%d\n", var->name, var->declarationLine);
    simplePrintWarning(buf);
    free(buf);
}

void messageUndefinedVar(char* var, int line){
    int size = 40 + strlen(var);
    char* buf = _createBuffer(size);
    snprintf(buf, size, "Variable: %s Undefined. # In line:%d\n", var,line);
    simplePrintError(buf);
    free(buf);
    exit(1);
}

void messageRedefinedVar(infoVar* var) {
    int size = 40 + strlen(var->name);
    char* buf = _createBuffer(size);
    snprintf(buf, size, "Variable: %s Redefined. # In line:%d\n", var->name, var->declarationLine);
    simplePrintError(buf);
    free(buf);
    exit(1);
}

